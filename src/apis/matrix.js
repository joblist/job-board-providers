/* matrix
	 docs:
	 - https://spec.matrix.org/latest/
 */

import { Provider, Job } from "../utils/models.js";

const providerId = "matrix";

const getJobs = async ({ hostname, companySlug = "", companyTitle = "" }) => {
	debugger;
	return [];
};

const api = new Provider({
	id: providerId,
	getJobs,
});

export default api;
export { getJobs };
