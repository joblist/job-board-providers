import providerDefinitions from "./board-providers.js";
import JoblistBoards from "./boards.js";
import JoblistBoard from "./board.js";
import JoblistBoardProvider from "./board-provider.js";
import JoblistBoardJob from "./board-job.js";

const componentDefinitions = {
	"joblist-boards": JoblistBoards,
	"joblist-board": JoblistBoard,
	"joblist-board-provider": JoblistBoardProvider,
	"joblist-board-job": JoblistBoardJob,
};

export default { componentDefinitions, providerDefinitions };
