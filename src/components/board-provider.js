export default class JoblistBoardProvider extends HTMLElement {
	model = [];

	getJobs = async () => {
		console.log("getJobs method not implemented for this job board provider");
	};

	async connectedCallback() {
		this.hostname = this.getAttribute("hostname");
		if (!this.hostname) return;

		try {
			this.model = await this.getJobs({
				hostname: this.hostname,
			});
		} catch (error) {
			console.log("Error", error);
		}

		if (this.model) {
			this.render();
		}
	}

	render() {
		const $component = this;
		if (this.model && this.model.length) {
			this.model.forEach(({ name, url, location }) => {
				if (!name || !url) return;
				let newJobItem = document.createElement("joblist-board-job");
				newJobItem.setAttribute("title", name);
				newJobItem.setAttribute("url", url);
				newJobItem.setAttribute("location", location);
				$component.append(newJobItem);
			});
		} else {
			let $noJob = document.createElement("joblist-board-job");
			$noJob.innerText = "There is currently no open job position";
			$component.append($noJob);
		}
	}
}
