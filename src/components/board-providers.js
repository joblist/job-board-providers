import JoblistBoardProvider from "./board-provider.js";

import personioApi from "../apis/personio.js";
import recruiteeApi from "../apis/recruitee.js";
import greenhouseApi from "../apis/greenhouse.js";
import smartrecruitersApi from "../apis/smartrecruiters.js";
import ashbyApi from "../apis/ashby.js";
import leverApi from "../apis/lever.js";
import workableApi from "../apis/workable.js";
import matrixApi from "../apis/matrix.js";

class JobListPersonio extends JoblistBoardProvider {
	id = personioApi.id;
	getJobs = personioApi.getJobs;
}

class JobListRecruitee extends JoblistBoardProvider {
	id = recruiteeApi.id;
	getJobs = recruiteeApi.getJobs;
}

class JobListSmartrecruiters extends JoblistBoardProvider {
	id = smartrecruitersApi.id;
	getJobs = smartrecruitersApi.getJobs;
}

class JobListGreenhouse extends JoblistBoardProvider {
	id = greenhouseApi.id;
	getJobs = greenhouseApi.getJobs;
}

class JobListAshby extends JoblistBoardProvider {
	id = ashbyApi.id;
	getJobs = ashbyApi.getJobs;
}

class JobListLever extends JoblistBoardProvider {
	id = leverApi.id;
	getJobs = leverApi.getJobs;
}

class JobListWorkable extends JoblistBoardProvider {
	id = workableApi.id;
	getJobs = workableApi.getJobs;
}
class JobListMatrix extends JoblistBoardProvider {
	id = matrixApi.id;
	getJobs = matrixApi.getJobs;
}

export default {
	"joblist-board-greenhouse": JobListGreenhouse,
	"joblist-board-personio": JobListPersonio,
	"joblist-board-recruitee": JobListRecruitee,
	"joblist-board-smartrecruiters": JobListSmartrecruiters,
	"joblist-board-ashby": JobListAshby,
	"joblist-board-lever": JobListLever,
	"joblist-board-workable": JobListWorkable,
	"joblist-board-matrix": JobListMatrix,
};
